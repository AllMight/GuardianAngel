// WORKS GOOD!

// libraries
#include <GSM.h>

// PIN Number
#define PINNUMBER "2536"

// APN data(SIM do Alex)
#define GPRS_APN       "umts"     // GPRS APN
#define GPRS_LOGIN     "alex"     // GPRS login
#define GPRS_PASSWORD  "alex"     // GPRS password

// initialize the library instance
GSMClient client;
GPRS gprs;
GSM gsmAccess;

// URL, path & port (for example: arduino.cc)
char server[] = "guardian-angel.northeurope.cloudapp.azure.com";   //
char path[] = "/events";     //
int port = 80; // port 80 is the default for HTTP

void setup() {
  // initialize serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println("Starting Arduino web client.");
  // connection state
  boolean notConnected = true;

  while (notConnected) {
    if ((gsmAccess.begin(PINNUMBER) == GSM_READY) &
        (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)) {
      notConnected = false;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }

  Serial.println("connecting...");

  // if you get a connection, report back via serial:
  if (client.connect(server, port)) {
    Serial.println("connected");
    // Make a HTTP request

//    Serial.println("Doing post...");
//    client.println("POST ");
//    client.print(path);
//    client.println(" HTTP/1.1");
//    client.println("Host:  guardian-angel.eastus.cloudapp.azure.com");
//    client.println("User-Agent: Arduino/1.0");
//    client.println("Connection: close");
//    //client.println("Content-Type: application/json; charset=utf-8");
//    //client.print("Content-Length: ");
//    //client.println(PostData.length());
//    //client.println();
//    //client.println("{device: 219c7b95-443b-4dab-b853-064f7a9bf8b4, severity: grave }");
//    //client.println("Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViMDJjY2Q3Y2FkNjdiMDAxMDFhZjZiNiIsInVzZXJuYW1lIjoiZ2F0ZXN0IiwiaWF0IjoxNTI2OTEwMjI3fQ.SIiDpQcT5Iz7V6At_cAZKRLoTtZBrjhnDmqGej7OYQU");
//    client.println("Content-Type:application/x-www-form-urlencoded");
//    //client.println("Host:Content-Type:application/json");
//    //client.println("{\"device\":\"931afce0-988a-4fa2-b1e7-3fa3eb778bd9\",\"severity\":\"grave\"}");
//    client.println("{\"device\":\"5b055dc329f44e0010e03096\",\"severity\":\"severe\"}");

    client.println();

     Serial.println("Doing get...");
    client.print("GET ");
    client.print(path);
    client.println(" HTTP/1.1");
    client.println("Content-Type: application/x-www-form-urlencoded;");
    client.println();
  } else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}

void loop() {
  // if there are incoming bytes available
  // from the server, read them and print them:
  if (client.available()) {
    char c = client.read();
    Serial.print(c);
  }

  // if the server's disconnected, stop the client:
  if (!client.available() && !client.connected()) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();

    for (;;)  // infinite cycle.
      ;
  }
}
