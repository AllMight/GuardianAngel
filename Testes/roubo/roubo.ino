#include <SoftwareSerial.h>
SoftwareSerial gps(11,12);  //RX,TX(11,12)
//arduino gps tx->11 gps rx->12
char str[70];
char *test="$GPRMC";
int temp,i,j=0;
String Lat;
long lati;
String lat_ori;
String long_ori;
String Long;                     
String Speed;
String hr;
String minut;
String secnd;
String dat;
String mnth;
String yar;
boolean Anticarjacking=false;

void setup() 
{
  Serial.begin(9600);
  gps.begin(9600);
  delay(100);
  Serial.println("\nSetup Done");
  pinMode(2,OUTPUT);
  pinMode(3,OUTPUT);
  pinMode(5,INPUT_PULLUP);
  pinMode(6,INPUT_PULLUP);
  
}

void loop() 
{
    gpsEvent();
    gpsPrint();
       } 
void gpsEvent()
{
    //Serial.println(gps.available());
    while(gps.available())  //checking serial data from GPS
    {
     // Serial.println("\nGeting gps data....");
      char inChar=(char)gps.read();
      
      str[i]=inChar;    //store data from GPS into str[]
      i++;
     // Serial.println(i);
      if(i<7)
      {
        if(str[i-1]!=test[i-1]) //checking for $GPRMC sentence
        {
          i=0;
        }
      }
      if(i>65)
      {
        temp=1;
        //break;
      }
      
      if(temp){
         Serial.print("\n");
         Serial.print(str);
        break;
      }
    }
}
void gpsPrint()
{
    long lats[10]; 
    lati=0; 
    Lat="";
    lat_ori="";
    long_ori="";
    Long="";                     
    Speed="";
    hr="";
    minut="";
    secnd="";
    dat="";
    mnth="";
    yar="";
      int x=6,comma=0;
      if(digitalRead(5)==LOW) {
        Anticarjacking=true;
        digitalWrite(2,HIGH); 
        Serial.print("\n Carjacking Mode");
      }
     if(digitalRead(6)==LOW) {
        Anticarjacking=false;
        digitalWrite(2,LOW); 
        digitalWrite(3,LOW);
      }
    if(temp)   
    {
      int str_length=i;
    
        // Extração do tempo
        do
        {
          if(str[x]==',') comma++;
        }while(comma!=1);
         if(comma==1)
         {
            x++;
            hr+=str[x++];
            hr+=str[x++];
            minut+=str[x++];
            minut+=str[x++];
            secnd+=str[x++];
            secnd+=str[x];
         }do
        {
          x++;
          if(str[x]==',') comma++;
        }while(comma!=3);
          
         if(comma=3)           // extração da latitude no formato dddmm.mmmm
        {
            x++;
         do
          {  
            int k;
            k=x;
            Lat+=str[x++];
            lati=(long)str[k];
            } while(str[x]!=',');
          comma=4;        
          x++;
          lat_ori=str[x];       //orientação da latitude
        }do
        {
          x++;
          if(str[x]==',') comma++;
        }while(comma!=5);
         if(comma=5)             //extração da longitude no formato dddmm.mmmm
        {
           x++;
         do
          {  
            Long+=str[x++];
          } while(str[x]!=',');
          comma=6;        
          x++;
          long_ori=str[x];         //orientação da longititude
        }do
        {
          x++;
          if(str[x]==',') comma++;
        }while(comma!=7);
        if(comma==7)             //extração da velocidade
        {   
            x++;
         
          do{  
            Speed+=str[x];
            x++;
          } while(str[x]!=',');
          comma=8;
        }do
        {
          x++;
          if(str[x]==',') comma++;
        }while(comma!=9);
        if(comma==9)
        {
          x++;
          dat+=str[x++];
          dat+=str[x++];
          mnth+=str[x++];
          mnth+=str[x++];
          yar+=str[x++];
          yar+=str[x];
        }
//       
//        Serial.print("\nLatitude:");
//        Serial.print(Lat);
//        Serial.print(" ");
//        Serial.print(lat_ori);
//        Serial.print("\nLongitude:");
//        Serial.print(Long);
//        Serial.print(" ");
//        Serial.print(long_ori);
          if(j<9){
          lats[j]=lati;
       
          j++;
        } else j=0;
     
     long diferenca= abs(lats[4]-lats[3]);
       Serial.print("\n a diferença e :");
       Serial.print(diferenca);
         Serial.print("\n");
        Serial.print("\n1 valor :");
        Serial.print(lats[4]);
         Serial.print("\n");
         Serial.print("\n2 valor :");
        Serial.print(lats[3]);
         Serial.print("\n");

        if(Anticarjacking){
          if(diferenca>3){
            Serial.println("O carro está a ser roubado");
            digitalWrite(3,HIGH);
          } else{
            Serial.println("Está tudo bem");
            digitalWrite(3,LOW);
          }
        }
        delay(2000);
        x=6;
       temp=0;
       i=0;
        }
}

