#include <GSM.h>
#define PINNUMBER ""
GSM gsmAccess;
GSM_SMS sms;

void setup() {
  Serial.begin(9600);
  Serial.println("SMS Messages Sender");
  
  boolean notConnected = true;
  while(notConnected){
    if(gsmAccess.begin(PINNUMBER)==GSM_READY){
      notConnected = false;
    }
    else{
      Serial.println("Not Connected");
      delay(1000);
    }
  }
  Serial.println("GSM initialized");
}
void loop() {
  //NUMERO PARA O QUAL VAI ENVIAR
  Serial.print("Enter a mobile number: ");
  char remoteNum[20];
  readSerial(remoteNum);
  Serial.println(remoteNum);
  //O CONTEUDO
  Serial.print("Now enter SMS content: ");
  char txtMsg[200];
  readSerial(txtMsg);
  Serial.println("Sending");
  Serial.println();
  Serial.println("Message:");
  Serial.println(txtMsg);
  //ENVIA A MENSSAGEM
  sms.beginSMS(remoteNum);
  sms.print(txtMsg);
  sms.endSMS();
  Serial.println("\nCOMPLEAT!\n");
}

//Guarda o numero que vou andar SMS e a menssagem
int readSerial(char result[]){ 
  int i = 0;
  while(1){
    while(Serial.available() > 0){
      char inChar = Serial.read();
      //Se inChar for nova linha, termina o array,limpa o buffer e sai da funçao
      if(inChar == '\n'){
        result[i] = '\0';
        Serial.flush();
        return 0;
      }
      //Se inChar for ASCII carrega para o array e incrementa o indice
      if(inChar != '\r'){
        result[i]=inChar;
        i++;
      }
    }
  }
}
