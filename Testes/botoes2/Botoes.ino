#define Botao_alarmeOn 22
#define Botao_alarmeOff 24
#define Botao_bluetooth 26
#define Botao_bateria 28
#define LEDbat1 30
#define LEDbat2 32
#define LEDbat3 34
#define LEDbat4 36
#define LEDbluetooth 38

int voltage = 0;
boolean Bluetooth_ON_OFF = false;

//Serial.begin(9600);

void readBattery(int alarm) {
  interface(alarm);
  int analog_value = analogRead(A0);
  voltage = (int)(analog_value * 4) / 1023; //com arredondamento  voltage = (analog_value * 9.0+511) / 1023.0;
  //carga=(voltage*100)/4; //carga de 0-100;
  //Serial.println("v= ");
  //Serial.print(input_voltage);
  //Serial.println("carga= ");
  //Serial.print(carga);
  //delay(300);
}

void pinSetup() {
  pinMode(LEDbat1, OUTPUT);         // Configura o pino  como saída;
  pinMode(LEDbat2, OUTPUT);         // Configura o pino  como saída;
  pinMode(LEDbat3, OUTPUT);         // Configura o pino  como saída;
  pinMode(LEDbat4, OUTPUT);         // Configura o pino  como saída;
  pinMode(LEDbluetooth, OUTPUT);    // Configura o pino  como saída;
  pinMode(Botao_alarmeOn, INPUT_PULLUP); // Configura pino x como entrada e habilita pull up interno;
  pinMode(Botao_alarmeOff, INPUT_PULLUP); // Configura pino x como entrada e habilita pull up interno;
  pinMode(Botao_bluetooth, INPUT_PULLUP); // Configura pino x como entrada e habilita pull up interno;
  pinMode(Botao_bateria, INPUT_PULLUP); // Configura pino x como entrada e habilita pull up interno;
}

void interface(int alarm) {
  if (digitalRead(Botao_alarmeOn) == LOW) { // Botão Pressionado;
    // Buzzer on
    tone(alarm, 2000);
  }
  if (digitalRead(Botao_alarmeOff) == LOW) { // Botão Pressionado;
    // Buzzer off
    noTone(alarm);
  }
  if (digitalRead(Botao_bluetooth) == LOW)  Bluetooth_ON_OFF = !Bluetooth_ON_OFF; //{
  /*delay(10);
    if(digitalRead(Botao_bluetooth) == LOW){
      //deixar o bluetooth ativo até que se volte a pressionar o botão
      //do{
      //ativar o bluetooth
      //  digitalWrite(LEDbluetooth, HIGH);
      //  }while(digitalRead(Botao_bluetooth)==HIGH);
      //desativar o bluetooth
      //digitalWrite(LEDbluetooth, LOW);
    //  }
    else{
      //ligar o bluetooth durante 10mn
      //ligar o led do bluetooth quando este for ativado, e desligar o led quando este for desativado
    }
    }*/
  if (digitalRead(Botao_bateria) == LOW) {
    if (voltage == 0) {
      digitalWrite(LEDbat1, LOW);
      digitalWrite(LEDbat2, LOW);
      digitalWrite(LEDbat3, LOW);
      digitalWrite(LEDbat4, LOW);
    }
    else if (voltage == 1) {
      digitalWrite(LEDbat1, HIGH);
      digitalWrite(LEDbat2, LOW);
      digitalWrite(LEDbat3, LOW);
      digitalWrite(LEDbat4, LOW);
    }
    else if (voltage == 2) {
      digitalWrite(LEDbat1, HIGH);
      digitalWrite(LEDbat2, HIGH);
      digitalWrite(LEDbat3, LOW);
      digitalWrite(LEDbat4, LOW);
    }
    else if (voltage == 3) {
      digitalWrite(LEDbat1, HIGH);
      digitalWrite(LEDbat2, HIGH);
      digitalWrite(LEDbat3, HIGH);
      digitalWrite(LEDbat4, LOW);
    }
    else {
      digitalWrite(LEDbat1, HIGH);
      digitalWrite(LEDbat2, HIGH);
      digitalWrite(LEDbat3, HIGH);
      digitalWrite(LEDbat4, HIGH);
    }
  }
}


