#include <Wire.h>
#include <GSM.h>

void setup() {
  Serial.begin(9600);
  Wire.begin();
  //startupGSM();
  startupAcelGyro();
}

void loop() {
  //testGSM();
  acel();
  gyro();
  Serial.println();
  delay(10);
}
