#include <Wire.h>

#define MPU 0x68
#define showG true
#define showA true

long Acx,Acy,Acz;
float volatile gAx,gAy,gAz;

long Gyx,Gyy,Gyz;
float volatile rotx,roty,rotz;

void startupAcelGyro(){
  Wire.beginTransmission(MPU);      //I2C MPU enderesso
  Wire.write(0x6B);                 //Aceder ao registo 6B - Alimentação
  Wire.write(0);                    //Registo Sleep = 0;
  Wire.endTransmission();
  Wire.beginTransmission(MPU);      //I2C MPU enderesso
  Wire.write(0x1B);                 //Aceder ao registo 1B - Configuração Gyro
  Wire.write(0);                    //Configurar para +/- 250 deg/s
  Wire.endTransmission();
  Wire.beginTransmission(MPU);      //I2C MPU enderesso
  Wire.write(0x1C);                 //Aceder ao registo 1C - Configuração Acel
  Wire.write(0);                    //Configurar para +/- 2g
  Wire.endTransmission();
}
void gyro(){
  Wire.beginTransmission(MPU);      //I2C MPU enderesso
  Wire.write(0x43);                 //iniciar o resgisto para a leitura da Gyro
  Wire.endTransmission();
  Wire.requestFrom(MPU,6);          //Registo Acell(3B-40)
  while(Wire.available() < 6);
  Gyx = Wire.read()<<8|Wire.read(); //Guarda os 2 bytes do inicio em Acx
  Gyy = Wire.read()<<8|Wire.read(); //Guarda os 2 bytes do meio em Acy
  Gyz = Wire.read()<<8|Wire.read(); //Guarda os 2 bytes do fim em Acz
  processGyro();
  if(showG == true){
    Serial.print("Gx = ");
    Serial.print(rotx);
    Serial.print(" ");
    Serial.print("Gy = ");
    Serial.print(roty);
    Serial.print(" ");
    Serial.print("Gz = ");
    Serial.print(rotz);
    Serial.print(" ");
  }
}
void acel() {
  Wire.beginTransmission(MPU);      //I2C MPU enderesso
  Wire.write(0x3B);                 //iniciar o resgisto para a leitura da Acel.
  Wire.endTransmission();
  Wire.requestFrom(MPU,6);          //Registo Acell(3B-40)
  while(Wire.available() < 6);
  Acx = Wire.read()<<8|Wire.read(); //Guarda os 2 bytes do inicio em Acx
  Acy = Wire.read()<<8|Wire.read(); //Guarda os 2 bytes do meio em Acy
  Acz = Wire.read()<<8|Wire.read(); //Guarda os 2 bytes do fim em Acz
  processAcel();
  if(showA == true){
    Serial.print("Ax = ");
    Serial.print(gAx);
    Serial.print(" ");
    Serial.print("Ay = ");
    Serial.print(gAy);
    Serial.print(" ");
    Serial.print("Az = ");
    Serial.print(gAz);
    Serial.print(" ");
  }
}
//--------------------|Parametrização dos valores|---------------------------
void processAcel(){
  gAx = (Acx/16384.0);
  gAy = (Acy/16384.0);
  gAz = (Acz/16384.0);
}
void processGyro(){
  rotx = Gyx/131.0;
  roty = Gyy/131.0;
  rotz = Gyz/131.0;
}
//---------------------------------------------------------------------------
