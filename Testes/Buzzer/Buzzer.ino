#define Buzzer 5
int output = 1500;

void setup() {
  Serial.begin(57600);
  noTone(Buzzer);
}

void loop() {
  if(Serial.available() > 0){
    output = Serial.parseInt();
    //output = map(dutyCycle,0,100,0,255);
  }
  tone(Buzzer,output);
  delay(50);
  noTone(Buzzer);
  delay(50);
}
