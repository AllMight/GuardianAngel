#include <SoftwareSerial.h>

SoftwareSerial Bluetooth(0,1);

String dataString;

void setup() {
  Bluetooth.begin(9600);
  Bluetooth.println("READY");
}

void loop() {
  if(Bluetooth.available()){
    dataString = Bluetooth.readString();
    Bluetooth.println(dataString);
  }
}
