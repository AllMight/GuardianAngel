#include <SoftwareSerial.h>

SoftwareSerial Bluetooth(0,1);

int Data;
int led = 13;

void setup() {
  Bluetooth.begin(9600);
  Bluetooth.println("BlueTooth On.\nPress 1 or 0 to blink LED");
  pinMode(led,OUTPUT);
}
void loop() {
  if(Bluetooth.available()){
    Data = Bluetooth.read();
    if(Data == '1'){
      digitalWrite(led,HIGH);
      Bluetooth.println("LED ON");
    }
    if(Data == '0'){
      digitalWrite(led,LOW);
      Bluetooth.println("LED OFF");
    }
  }
  delay(100);
}
