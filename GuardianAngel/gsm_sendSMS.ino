
#include <GSM.h>

#define PINNUMBER "2536"

volatile int remoteNum = "915477882";
volatile String txtMsg = "ola";
volatile boolean msg = true;

// initialize the library instance
GSM gsmAccess;
GSM_SMS sms;

void setup() {
  // initialize serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println("SMS Messages Sender");

  // connection state
  boolean notConnected = true;

  // Start GSM shield
  while (notConnected) {
    if (gsmAccess.begin(PINNUMBER) == GSM_READY) {
      notConnected = false;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }

  Serial.println("GSM initialized");
}

void loop() {

//  If the user needs to change the default mobile number for another one

//  Serial.print("Enter a mobile number: ");
//  char remoteNum[20];  // telephone number to send sms
//  readSerial(remoteNum);
//  Serial.println(remoteNum);

//  If the user needs to change the default SMS content for another one

//  Serial.print("Now, enter SMS content: ");
//  char txtMsg[200];
//  readSerial(txtMsg);
  
  Serial.println("SENDING");
  Serial.println();
  Serial.println("Message:");
 
  for (int nI = 0; nI < txtMsg.length(); nI++)
      Serial.print(txtMsg[nI]);
  Serial.println("");
   
  // send the message
  sms.beginSMS(remoteNum);
  for (int nI = 0; nI < txtMsg.length(); nI++)
  {
      sms.print(txtMsg[nI]);
  }
  sms.endSMS();
  Serial.println("\nSENDED WITH SUCCESS!\n");

  for(;;);
}

/*
  Read input serial
 */
//int readSerial(char result[]) {
//  int i = 0;
//  while (1) {
//    while (Serial.available() > 0) {
//      char inChar = Serial.read();
//      if (inChar == '\n') {
//        result[i] = '\0';
//        Serial.flush();
//        return 0;
//      }
//      if (inChar != '\r') {
//        result[i] = inChar;
//        i++;
//      }
//    }
//  }
//}

