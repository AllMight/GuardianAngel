#define Botao_alarmeOn 22
#define Botao_alarmeOff 24
#define Botao_bluetooth 26
#define Botao_bateria 28
#define LEDbat1 30
#define LEDbat2 32
#define LEDbat3 36//deveria ser 34 mas está soldado no 36
#define LEDbat4 38//deveria ser 36 mas está soldado no 38
#define LEDbluetooth 40//deveria ser 38 mas está soldado no 40
//led amarelo está no pino 34
float voltage = 0;
boolean Bluetooth_ON_OFF = false;
int carga;
//Serial.begin(9600);

void readBattery(int alarm) {
  interface(alarm);
  voltage = analogRead(A3); //(5/1023)=(1.15/x) <=> x=235-> maximo (5/1023)=(0.92/y) <=> y=188->80% do  maximo 
  carga=(int)((voltage*100)/235);
  Serial.println("\nv= ");
  Serial.print(voltage);
  //Serial.println("carga= ");
  //Serial.print(carga);
  //delay(300);
}

void pinSetup() {
  pinMode(LEDbat1, OUTPUT);         // Configura o pino  como saída;
  pinMode(LEDbat2, OUTPUT);         // Configura o pino  como saída;
  pinMode(LEDbat3, OUTPUT);         // Configura o pino  como saída;
  pinMode(LEDbat4, OUTPUT);         // Configura o pino  como saída;
  pinMode(LEDbluetooth, OUTPUT);    // Configura o pino  como saída;
  pinMode(34,OUTPUT);
  pinMode(Botao_alarmeOn, INPUT_PULLUP); // Configura pino x como entrada e habilita pull up interno;
  pinMode(Botao_alarmeOff, INPUT_PULLUP); // Configura pino x como entrada e habilita pull up interno;
  pinMode(Botao_bluetooth, INPUT_PULLUP); // Configura pino x como entrada e habilita pull up interno;
  pinMode(Botao_bateria, INPUT_PULLUP); // Configura pino x como entrada e habilita pull up interno;
}

void interface(int alarm) {
  if (digitalRead(Botao_alarmeOn) == LOW) { // Botão Pressionado;
    // Buzzer on
    Serial.print("\n alarme on");
    tone(alarm, 2000);
    digitalWrite(34,HIGH);
  }
  if (digitalRead(Botao_alarmeOff) == LOW) { // Botão Pressionado;
    // Buzzer off
    noTone(alarm);
    digitalWrite(34,LOW);
  }
  if (digitalRead(Botao_bluetooth) == LOW) {
    Bluetooth_ON_OFF = !Bluetooth_ON_OFF; 
    if(Bluetooth_ON_OFF==true) digitalWrite(LEDbluetooth,HIGH);
    else  digitalWrite(LEDbluetooth,LOW);
    BluetoothModule();  
  /*delay(10);
    if(digitalRead(Botao_bluetooth) == LOW){
      //deixar o bluetooth ativo até que se volte a pressionar o botão
      //do{
      //ativar o bluetooth
      //  digitalWrite(LEDbluetooth, HIGH);
      //  }while(digitalRead(Botao_bluetooth)==HIGH);
      //desativar o bluetooth
      //digitalWrite(LEDbluetooth, LOW);
    //  }
    else{
      //ligar o bluetooth durante 10mn
      //ligar o led do bluetooth quando este for ativado, e desligar o led quando este for desativado
    }
    }*/
  if (digitalRead(Botao_bateria) == LOW) {
    if (voltage <= 188) {
      digitalWrite(LEDbat1, LOW);
      digitalWrite(LEDbat2, LOW);
      digitalWrite(LEDbat3, LOW);
      digitalWrite(LEDbat4, LOW);
    }
    else if (voltage >188 && voltage<=200) {
      digitalWrite(LEDbat1, HIGH);
      digitalWrite(LEDbat2, LOW);
      digitalWrite(LEDbat3, LOW);
      digitalWrite(LEDbat4, LOW);
    }
    else if (voltage >200 && voltage <=212) {
      digitalWrite(LEDbat1, HIGH);
      digitalWrite(LEDbat2, HIGH);
      digitalWrite(LEDbat3, LOW);
      digitalWrite(LEDbat4, LOW);
    }
    else if (voltage >212 && voltage <=224) {
      digitalWrite(LEDbat1, HIGH);
      digitalWrite(LEDbat2, HIGH);
      digitalWrite(LEDbat3, HIGH);
      digitalWrite(LEDbat4, LOW);
    }
    else {
      digitalWrite(LEDbat1, HIGH);
      digitalWrite(LEDbat2, HIGH);
      digitalWrite(LEDbat3, HIGH);
      digitalWrite(LEDbat4, HIGH);
    }
  }else{
      digitalWrite(LEDbat1, LOW);
      digitalWrite(LEDbat2, LOW);
      digitalWrite(LEDbat3, LOW);
      digitalWrite(LEDbat4, LOW);
  }
}


