#include <Wire.h>
#include <SPI.h>
#include <SparkFunLSM6DS3.h>
#include <SoftwareSerial.h>

#define GyroCrashCriteria 100                   //Alterado depois de testes  
#define AcelCrashCriteria 100                   //Alterado depois de testes
#define interruptCrashPin 2
#define crashSendRequestPin 7
#define Alarm             5

String crashMessage = "";

LSM6DS3 sensor(I2C_MODE,0x6A);
SoftwareSerial Bluetooth(0,1); //rx/tx // alterar!//bluetooth config

void setup(){
  pinSetup();

  noTone(Alarm);                          //Alarme começa desligado
  pinMode(crashSendRequestPin,OUTPUT);    //sinal responsavel por indicar acidente
  pinMode(interruptCrashPin,INPUT);       //sinal responsavel pela rotina. A qualquer momento do codigo pode ser gerada a função
  attachInterrupt(digitalPinToInterrupt(interruptCrashPin),CrashState,RISING);
  
                                          //Definições iniciais do GIROSCOPIO
  sensor.settings.gyroEnabled = 1;        //Enable do Giroscopio
  sensor.settings.gyroRange = 2000;       //Alcançe do Giroscopio deg/s
  sensor.settings.gyroSampleRate = 833;   //Frequencia das samples (Hz)
  sensor.settings.gyroBandWidth = 200;
  sensor.settings.gyroFifoEnabled = 1;
  sensor.settings.gyroFifoDecimation = 1;
                                          //Definições iniciais do ACELEROMETRO
  sensor.settings.accelEnabled = 1;       //Enable do acelerometro
  sensor.settings.accelRange = 16;        //Alcançe do acelerometro g
  sensor.settings.accelSampleRate = 833;  //Frequencia das samples (Hz)
  sensor.settings.accelBandWidth = 200;
  sensor.settings.accelFifoEnabled = 1;
  sensor.settings.accelFifoDecimation = 1;

  sensor.settings.tempEnabled = 1;

  sensor.settings.commMode = 1;
                                          //Definições iniciais do FIFO
  sensor.settings.fifoThreshold = 100;
  sensor.settings.fifoSampleRate = 50;
  sensor.settings.fifoModeWord = 6;
                                          //FIFO mode.  Can be:
                                          //  0 (Bypass mode, FIFO off)
                                          //  1 (Stop when full)
                                          //  3 (Continuous during trigger)
                                          //  4 (Bypass until trigger)
                                          //  6 (Continous mode)
  Serial.begin(57600);
  delay(1000);
  Serial.println("Fim de configuração \n");
  
  Serial.print("Begining Bluetooth...");
  Bluetooth.begin(9600);                  //bluetooth ativo
  Serial.println("Done :)");
  
  Serial.print("Configurar sensor....");
  if(sensor.begin() != 0){
    Serial.println("Erro após o reset\n");
  }
  else{
    Serial.println("Pronto\n");  
  }  
  Serial.print("Iniciar FIFO...");
  sensor.fifoBegin();
  Serial.println("Pronto \n");
  
  Serial.print("Limpar FIFO...");
  sensor.fifoClear();
  Serial.println("Pronto!\n");
}

void loop() {
  readBattery(Alarm);
  
  float temp,gX,gY,gZ;                          //Variavel que contem dados
  
  while((sensor.fifoGetStatus() & 0x8000)==0);  //Espera pela watermark. Espera até ter 100 amostras
  while((sensor.fifoGetStatus() & 0x1000)==0){  //Esperar ate o FIFO estiver livre. Por motivos de sincronização
    temp = sensor.calcGyro(sensor.fifoRead());
    Evaluator(temp,"Gyro");
    //Serial.print(temp);
    //Serial.print(" ");
    
    temp = sensor.calcGyro(sensor.fifoRead());
    Evaluator(temp,"Gyro");
    //Serial.print(temp);
    //Serial.print(" ");
    
    temp = sensor.calcGyro(sensor.fifoRead());
    Evaluator(temp,"Gyro");
    //Serial.println(temp);
 
    gX = sensor.calcAccel(sensor.fifoRead());
    //Serial.print(gX);
    Evaluator(gX,"Acel");
    //Serial.print(" ");
  
    gY = sensor.calcAccel(sensor.fifoRead());
    //Serial.print(gY);
    //Evaluator(gY,"Acel");
    //Serial.print(" ");
  
    gZ = sensor.calcAccel(sensor.fifoRead());
    //Serial.println(gZ);
    //Evaluator(gZ,"Acel");
    delay(10);                                  //Esperar até o buffer estar livre
  }
}

void CrashState(){
  //GSM GPS
  tone(Alarm,2000);
  //Serial.print("CRASH");
  digitalWrite(crashSendRequestPin,LOW);
  //noTone(Alarm);
}

void Evaluator(float value,String type){
  if(type == "Gyro"){
    if(value >= GyroCrashCriteria)  digitalWrite(crashSendRequestPin,HIGH);
    else digitalWrite(crashSendRequestPin,LOW);
  }
  else{
    if(value >= AcelCrashCriteria)  digitalWrite(crashSendRequestPin,HIGH);
    else digitalWrite(crashSendRequestPin,LOW);
  }
}

String BluetoothModule() {
  String BlueData;
  if(Bluetooth.available()){
    BlueData = Bluetooth.readString();
  }
  return BlueData;
}
